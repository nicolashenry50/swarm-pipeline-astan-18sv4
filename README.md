Swarm pipeline Astan 18SV4
==========================

This project contains the scripts producing the OTU table used in
Caracciolo et al. 202X paper. You will find details about the pipeline
and some statistics about the OTU table clicking on this
[link](https://nicolashenry50.gitlab.io/swarm-pipeline-astan-18sv4)
